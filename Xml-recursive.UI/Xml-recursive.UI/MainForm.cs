﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DirectoryScanner;
using Models.Info;
using XmlFileUtils;

namespace Xml_recursive.UI
{
  public partial class MainForm : Form
  {
    private string _scanSelectedPath;
    private readonly Timer _timerForUiRender;

    private DirectoryInfoEntity _currentDirectoryForRender;
    private readonly LogicManager _logicManager;

    public MainForm()
    {
      _timerForUiRender = new Timer();
      _timerForUiRender.Interval = 1000;
      _timerForUiRender.Tick += TimerForUiRenderOnTick;
      _logicManager = new LogicManager(OnScanningItemFound);
      InitializeComponent();
    }

    private void TimerForUiRenderOnTick(object sender, EventArgs e)
    {
      if (_currentDirectoryForRender == null)
      {
        return;
      }
      
      BeginInvoke((Action)(() =>
      {
        BuildTree(_currentDirectoryForRender, treeView1.Nodes);
      }));
    }


    private void startScanningBtn_Click(object sender, EventArgs e)
    {
      treeView1.Nodes.Clear();
      _timerForUiRender.Start();
      try
      {
        _logicManager.StartScanningTread(_scanSelectedPath, OnScanningEnded);
      }
      catch (Exception exception)
      {
        AppendTextToOutputLog(exception.Message);
      }
    }

    private void OnScanningItemFound(DirectoryInfoEntity result)
    {
      _currentDirectoryForRender = result;
    }

    private void OnScanningEnded(DirectoryInfoEntity result)
    {
      _currentDirectoryForRender = result;
      _timerForUiRender.Stop();
      TimerForUiRenderOnTick(null, null);

      AppendTextToOutputLog($@"Scanning for {result.Name} Ended");
    }

    private void BuildTree(DirectoryInfoEntity directoryInfo, TreeNodeCollection treeNodeCollection)
    {
      TreeNode curNode = null;
      if (treeNodeCollection.ContainsKey(directoryInfo.Name))
      {
        curNode = treeNodeCollection[directoryInfo.Name];
      }
      else
      {
        curNode = treeNodeCollection.Add(directoryInfo.Name, directoryInfo.Name);
      }

      var childFiles = directoryInfo.ChildFiles;
      foreach (FileInfoEntity file in childFiles)
      {
        if (!curNode.Nodes.ContainsKey(file.Name))
        {
          curNode.Nodes.Add(file.Name, file.Name);
        }
      }

      var childFolders = directoryInfo.ChildDirectories;
      foreach (DirectoryInfoEntity subdir in childFolders)
      {
        BuildTree(subdir, curNode.Nodes);
      }
    }

    private void showFolderDialogBtn_Click(object sender, EventArgs e)
    {
      var result = this.folderBrowserDialog1.ShowDialog();
      while (result != DialogResult.OK || result == DialogResult.Cancel)
      {
        result = this.folderBrowserDialog1.ShowDialog();
      }

      if (result == DialogResult.Cancel)
      {

        return;
      }

      _scanSelectedPath = this.folderBrowserDialog1.SelectedPath;
      this.selecetedFolderPathLabel.Text = _scanSelectedPath;
      this.folderBrowserDialog1.Reset();
    }

    private void choseFolderForXmlFileBtn_Click(object sender, EventArgs e)
    {
      var result = this.folderBrowserDialog1.ShowDialog();
      while (result != DialogResult.OK || result == DialogResult.Cancel)
      {
        result = this.folderBrowserDialog1.ShowDialog();
      }

      if (result == DialogResult.Cancel)
      {

        return;
      }

      this.selectedFolderForXmlLabel.Text = this.folderBrowserDialog1.SelectedPath;
      try
      {
        _logicManager.SetPathForXmlResult(this.folderBrowserDialog1.SelectedPath);
      }
      catch (Exception exception)
      {
        AppendTextToOutputLog(exception.Message);
      }
      this.folderBrowserDialog1.Reset();
    }

    private void AppendTextToOutputLog(string text)
    {
      BeginInvoke((Action)(() =>
      {
        this.richTextBox1.Text += text;
        this.richTextBox1.Text += Environment.NewLine;
      }));
    }
  }
}
