﻿namespace Xml_recursive.UI
{
  partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.treeView1 = new System.Windows.Forms.TreeView();
      this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
      this.showFolderDialogBtn = new System.Windows.Forms.Button();
      this.startScanningBtn = new System.Windows.Forms.Button();
      this.choseFolderForXmlFileBtn = new System.Windows.Forms.Button();
      this.selecetedFolderPathLabel = new System.Windows.Forms.Label();
      this.selectedFolderForXmlLabel = new System.Windows.Forms.Label();
      this.richTextBox1 = new System.Windows.Forms.RichTextBox();
      this.SuspendLayout();
      // 
      // treeView1
      // 
      this.treeView1.Location = new System.Drawing.Point(12, 12);
      this.treeView1.Name = "treeView1";
      this.treeView1.Size = new System.Drawing.Size(414, 426);
      this.treeView1.TabIndex = 0;
      // 
      // showFolderDialogBtn
      // 
      this.showFolderDialogBtn.Location = new System.Drawing.Point(625, 12);
      this.showFolderDialogBtn.Name = "showFolderDialogBtn";
      this.showFolderDialogBtn.Size = new System.Drawing.Size(163, 63);
      this.showFolderDialogBtn.TabIndex = 1;
      this.showFolderDialogBtn.Text = "Select folder for scan";
      this.showFolderDialogBtn.UseVisualStyleBackColor = true;
      this.showFolderDialogBtn.Click += new System.EventHandler(this.showFolderDialogBtn_Click);
      // 
      // startScanningBtn
      // 
      this.startScanningBtn.Location = new System.Drawing.Point(625, 151);
      this.startScanningBtn.Name = "startScanningBtn";
      this.startScanningBtn.Size = new System.Drawing.Size(163, 54);
      this.startScanningBtn.TabIndex = 2;
      this.startScanningBtn.Text = "Scan";
      this.startScanningBtn.UseVisualStyleBackColor = true;
      this.startScanningBtn.Click += new System.EventHandler(this.startScanningBtn_Click);
      // 
      // choseFolderForXmlFileBtn
      // 
      this.choseFolderForXmlFileBtn.Location = new System.Drawing.Point(625, 81);
      this.choseFolderForXmlFileBtn.Name = "choseFolderForXmlFileBtn";
      this.choseFolderForXmlFileBtn.Size = new System.Drawing.Size(163, 64);
      this.choseFolderForXmlFileBtn.TabIndex = 3;
      this.choseFolderForXmlFileBtn.Text = "Select dist folder for xml file";
      this.choseFolderForXmlFileBtn.UseVisualStyleBackColor = true;
      this.choseFolderForXmlFileBtn.Click += new System.EventHandler(this.choseFolderForXmlFileBtn_Click);
      // 
      // selecetedFolderPathLabel
      // 
      this.selecetedFolderPathLabel.AutoSize = true;
      this.selecetedFolderPathLabel.Location = new System.Drawing.Point(446, 12);
      this.selecetedFolderPathLabel.Name = "selecetedFolderPathLabel";
      this.selecetedFolderPathLabel.Size = new System.Drawing.Size(112, 13);
      this.selecetedFolderPathLabel.TabIndex = 4;
      this.selecetedFolderPathLabel.Text = "Selected Folder Path: ";
      // 
      // selectedFolderForXmlLabel
      // 
      this.selectedFolderForXmlLabel.AutoSize = true;
      this.selectedFolderForXmlLabel.Location = new System.Drawing.Point(446, 93);
      this.selectedFolderForXmlLabel.Name = "selectedFolderForXmlLabel";
      this.selectedFolderForXmlLabel.Size = new System.Drawing.Size(159, 13);
      this.selectedFolderForXmlLabel.TabIndex = 5;
      this.selectedFolderForXmlLabel.Text = "Selected Folder FOR XML Path:";
      // 
      // richTextBox1
      // 
      this.richTextBox1.Location = new System.Drawing.Point(432, 211);
      this.richTextBox1.Name = "richTextBox1";
      this.richTextBox1.Size = new System.Drawing.Size(356, 227);
      this.richTextBox1.TabIndex = 6;
      this.richTextBox1.Text = "";
      // 
      // MainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(800, 450);
      this.Controls.Add(this.richTextBox1);
      this.Controls.Add(this.selectedFolderForXmlLabel);
      this.Controls.Add(this.selecetedFolderPathLabel);
      this.Controls.Add(this.choseFolderForXmlFileBtn);
      this.Controls.Add(this.startScanningBtn);
      this.Controls.Add(this.showFolderDialogBtn);
      this.Controls.Add(this.treeView1);
      this.Name = "MainForm";
      this.Text = "MainForm";
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TreeView treeView1;
    private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    private System.Windows.Forms.Button showFolderDialogBtn;
    private System.Windows.Forms.Button startScanningBtn;
    private System.Windows.Forms.Button choseFolderForXmlFileBtn;
    private System.Windows.Forms.Label selecetedFolderPathLabel;
    private System.Windows.Forms.Label selectedFolderForXmlLabel;
    private System.Windows.Forms.RichTextBox richTextBox1;
  }
}

