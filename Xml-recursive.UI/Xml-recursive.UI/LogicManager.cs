﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DirectoryScanner;
using Models.Exceptions;
using Models.Info;
using Models.ThreadUtils;
using XmlFileUtils;

namespace Xml_recursive.UI
{
  public class LogicManager
  {
    private readonly Action<DirectoryInfoEntity> _onNewItemFoundActionForUi;
    private string _xmlSelectedPath;
    private readonly QueueBufferManager _queueBufferManagerForXml;

    public LogicManager(Action<DirectoryInfoEntity> onNewItemFoundActionForUi)
    {
      _onNewItemFoundActionForUi = onNewItemFoundActionForUi;
      _queueBufferManagerForXml = new QueueBufferManager();
    }

    public void StartScanningTread(string scanSelectedPath, Action<DirectoryInfoEntity> onScanningEndCallback)
    {
      if (string.IsNullOrWhiteSpace(scanSelectedPath))
      {
        throw new IncorrectParametersException($"Path for scanning folder have to be valid. Input data: {scanSelectedPath}");
      }

      if (string.IsNullOrWhiteSpace(_xmlSelectedPath))
      {
        throw new IncorrectParametersException($"Path for xml result have to be valid. Input data: {_xmlSelectedPath}");
      }

      var scanningThread = new ThreadWrapper<DirectoryInfoEntity>(scanSelectedPath, ScanningThreadWorker, onScanningEndCallback, true);
      scanningThread.Start();
    }

    public void SetPathForXmlResult(string xmlResultPath)
    {
      if (string.IsNullOrWhiteSpace(xmlResultPath))
      {
        throw new IncorrectParametersException($"Path for xml result have to be valid. Input data: {xmlResultPath}");
      }
      _xmlSelectedPath = xmlResultPath;
    }

    private DirectoryInfoEntity ScanningThreadWorker(object scanSelectedPath)
    {
      var scanPath = scanSelectedPath as string;
      if (string.IsNullOrWhiteSpace(scanPath))
      {
        return null;
      }

      var scanManager = new ScannerManager(scanPath);
      var uiEventHandler = new ScannerManager.NewItemFoundEvent(_onNewItemFoundActionForUi);
      var xmlEventHandler = new ScannerManager.NewItemFoundEvent(StartSaveToXmlThread);

      scanManager.OnNewItemFound += uiEventHandler;
      scanManager.OnNewItemFound += xmlEventHandler;

      var result = scanManager.ScanDirectory();

      scanManager.OnNewItemFound -= uiEventHandler;
      scanManager.OnNewItemFound -= xmlEventHandler;
      return result;
    }

    private void StartSaveToXmlThread(DirectoryInfoEntity resultFile)
    {
      _queueBufferManagerForXml.SetItemInWaitingQueue(resultFile, _xmlSelectedPath);
    }
  }
}
