﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Exceptions;
using Models.Info;

namespace DirectoryScanner
{
  public class ScannerManager
  {
    public delegate void NewItemFoundEvent(DirectoryInfoEntity directoryInfoEntity);
    public event NewItemFoundEvent OnNewItemFound;

    private DirectoryInfoEntity _rootEntity;
    private readonly string _dirPath;

    public ScannerManager(string dirPath)
    {
      if (!Directory.Exists(dirPath))
      {
        throw new IncorrectParametersException(string.Format("No directory exits on this path :{0}", dirPath));
      }

      _dirPath = dirPath;
    }

    public DirectoryInfoEntity ScanDirectory()
    {
      var dirInfo = new DirectoryInfo(_dirPath);
      var dirEntity = dirInfo.ToInfoEntity();
      if (_rootEntity == null)
      {
        _rootEntity = dirEntity;
      }
      return ProcessDirectory(dirInfo, dirEntity);
    }

    private DirectoryInfoEntity ProcessDirectory(DirectoryInfo directoryInfo, DirectoryInfoEntity dirEntity)
    {
      IEnumerable<DirectoryInfo> childFolders;
      try
      {
        childFolders = directoryInfo.EnumerateDirectories();
      }
      catch (Exception)
      {
        //probably have no access
        childFolders = new List<DirectoryInfo>();
      }

      foreach (var childFolder in childFolders)
      {
        var childFolderEntity = childFolder.ToInfoEntity();
        dirEntity.RegisterNewDirectory(childFolderEntity);
        ProduceEventFoundNewItem(_rootEntity);

        ProcessDirectory(childFolder, childFolderEntity);
      }

      IEnumerable<FileInfo> childFiles;
      try
      {
        childFiles = directoryInfo.EnumerateFiles();
      }
      catch (Exception)
      {
        //probably have no access
        childFiles = new List<FileInfo>();
      }

      foreach (var childFile in childFiles)
      {
        var childFileInfoEntity = childFile.ToInfoEntity();
        dirEntity.RegisterNewFile(childFileInfoEntity);
        ProduceEventFoundNewItem(_rootEntity);
      }

      return dirEntity;
    }

    private void ProduceEventFoundNewItem(DirectoryInfoEntity rootEntity)
    {
      OnNewItemFound?.Invoke(rootEntity);
    }
  }
}
