﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Models.Info;

namespace DirectoryScanner
{
  public static class InfoUtils
  {
    private static readonly WindowsIdentity CurrentIdentity;

    static InfoUtils()
    {
      CurrentIdentity = WindowsIdentity.GetCurrent();
    }

    public static DirectoryInfoEntity ToInfoEntity(this DirectoryInfo directoryInfo)
    {
      var owner = "undefined";
      FileSystemRights rights = 0;
      try
      {
        var accessControl = directoryInfo.GetAccessControl();
        var user = accessControl.GetOwner(typeof(NTAccount));
        owner = user.Value;
        rights = GetFileSystemRights(accessControl);
      }
      catch (Exception)
      {
        // ignored
      }

      var dirInfoEntity = new DirectoryInfoEntity(directoryInfo, owner, rights);
      return dirInfoEntity;
    }

    public static FileInfoEntity ToInfoEntity(this FileInfo fileInfo)
    {
      var owner = "undefined";
      FileSystemRights rights = 0;
      try
      {
        var accessControl = fileInfo.GetAccessControl();
        var user = accessControl.GetOwner(typeof(NTAccount));
        owner = user.Value;
        rights = GetFileSystemRights(accessControl);
      }
      catch (Exception)
      {
        // ignored
      }

      var fileSize = fileInfo.Length;
      var dirInfoEntity = new FileInfoEntity(fileInfo, owner, rights, fileSize);
      return dirInfoEntity;
    }

    private static FileSystemRights GetFileSystemRights(FileSystemSecurity accessControl)
    {
      FileSystemRights rights = 0;
      var accessRules = accessControl.GetAccessRules(true, true, typeof(System.Security.Principal.NTAccount));
      foreach (FileSystemAccessRule rule in accessRules)
      {
        if (rule.IdentityReference.ToString() != CurrentIdentity.Name &&
            (CurrentIdentity.Groups == null || !CurrentIdentity.Groups.Contains(rule.IdentityReference)))
        {
          continue;
        }

        rights = rights | rule.FileSystemRights;
      }

      return rights;
    }
  }
}
