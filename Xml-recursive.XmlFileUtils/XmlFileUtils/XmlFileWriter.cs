﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Models.Info;

namespace XmlFileUtils
{
  public class XmlFileWriter
  {
    private const string XmlFileNamePattern = "scanResult_for_{0}.xml";
    private const string RootElementName = "root";

    public static string GetFileName(string destPath, DirectoryInfoEntity directoryInfoEntity)
    {
      var folderName = directoryInfoEntity.Name;
      var xmlFileName = string.Format(XmlFileNamePattern, folderName);
      return Path.Combine(destPath, xmlFileName);
    }

    public void WriteToXmlFile(string filePath, DirectoryInfoEntity directoryInfoEntity)
    {
      XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
      xmlWriterSettings.Indent = true;
      xmlWriterSettings.NewLineOnAttributes = true;

      //init file, if it need
      if (!File.Exists(filePath))
      {
        XmlWriter xmlWriter = XmlWriter.Create(filePath, xmlWriterSettings);
        xmlWriter.WriteStartDocument();
        xmlWriter.WriteStartElement(RootElementName);

        xmlWriter.WriteEndElement();
        xmlWriter.WriteEndDocument();
        xmlWriter.Flush();
        xmlWriter.Close();
        xmlWriter.Dispose();
      }


      var xmlDocument = XDocument.Load(filePath);
      var rootElement = xmlDocument.Element(RootElementName);
      if (rootElement == null)
      {
        throw new Exception("Xml writing wrong. File exist, but no root node");
      }

      FillElements(directoryInfoEntity, rootElement);

      xmlDocument.Save(filePath);
    }

    private void FillElements(DirectoryInfoEntity directoryInfoEntity, XElement element)
    {
      var targetFolderElement = element.Descendants(XmlMapperForInfoEntity.FolderElemName)
        .FirstOrDefault(x => (string) x.Attribute(XmlMapperForInfoEntity.NameAttributeName) == directoryInfoEntity.Name);

      if (targetFolderElement == null)
      {
        targetFolderElement = XmlMapperForInfoEntity.ConvertToXElement(directoryInfoEntity);
        element.Add(targetFolderElement);
      }

      targetFolderElement.SetAttributeValue(XmlMapperForInfoEntity.SizeAttributeName, directoryInfoEntity.Size);

      var childFolders = directoryInfoEntity.ChildDirectories;
      foreach (var childFolder in childFolders)
      {
        FillElements(childFolder, targetFolderElement);
      }

      var childFiles = directoryInfoEntity.ChildFiles;
      foreach (var fileInfoEntity in childFiles)
      {
        var targetFileElement = targetFolderElement.Descendants(XmlMapperForInfoEntity.FileElemName)
          .FirstOrDefault(x => (string)x.Attribute(XmlMapperForInfoEntity.NameAttributeName) == fileInfoEntity.Name);
        if (targetFileElement == null)
        {
          var fileElement = XmlMapperForInfoEntity.ConvertToXElement(fileInfoEntity);
          targetFolderElement.Add(fileElement);
        }
      }
    }
  }
}