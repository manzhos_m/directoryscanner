﻿using System.Diagnostics;
using System.Threading;
using Models.Collections;
using Models.Info;

namespace XmlFileUtils
{
  public class QueueBufferManager
  {
    public const int MaxBuffer = 10;
    private static bool _isRunning = false;
    private readonly object _syncForQueue = new object();
    private readonly object _syncForFlag = new object();

    private QueueWithLastField<DirectoryInfoEntity> _queue;

    private string _resultSavingDir;

    public QueueBufferManager()
    {
      _queue = new QueueWithLastField<DirectoryInfoEntity>();
    }

    public void SetItemInWaitingQueue(DirectoryInfoEntity directoryInfoEntity, string resultSavingDir)
    {
      lock (_syncForQueue)
      {
        _resultSavingDir = resultSavingDir;
        _queue.Enqueue(directoryInfoEntity);
      }

      lock (_syncForFlag)
      {
        if (!_isRunning)
        {
          StartThreadForSaving();
        }
      }
    }

    private void StartThreadForSaving()
    {
      var thread = new Thread(StartXmlSavingProcess) { IsBackground = true };
      thread.Start();
    }

    private void StartXmlSavingProcess()
    {
      lock (_syncForFlag)
      {
        if (_isRunning)
        {
          Debug.WriteLine("QueueBufferManager.StartXmlSavingProcess => already started");
          return;
        }

        Debug.WriteLine("QueueBufferManager.StartXmlSavingProcess => started");
        _isRunning = true;
      }

      DirectoryInfoEntity itemFromQueue;
      while (TryGetItemFromQueue(out itemFromQueue))
      {
        RunXmlSaving(itemFromQueue);
      }

      lock (_syncForFlag)
      {
        Debug.WriteLine("QueueBufferManager.StartXmlSavingProcess => stopped");
        _isRunning = false;
      }

      var currentThread = Thread.CurrentThread;
      currentThread.Abort();
    }

    private void RunXmlSaving(DirectoryInfoEntity directoryInfoEntity)
    {
      var filePath = XmlFileWriter.GetFileName(_resultSavingDir, directoryInfoEntity);
      var xmlFileWriter = new XmlFileWriter();
      xmlFileWriter.WriteToXmlFile(filePath, directoryInfoEntity);
    }


    private bool TryGetItemFromQueue(out DirectoryInfoEntity itemFromQueue)
    {
      itemFromQueue = null;
      lock (_syncForQueue)
      {
        if (_queue.Count == 0)
        {
          return false;
        }

        //skip all and get the last
        if (_queue.Count >= MaxBuffer)
        {
          itemFromQueue = _queue.Last;
          _queue = new QueueWithLastField<DirectoryInfoEntity>();
          return true;
        }

        itemFromQueue = _queue.Dequeue();
        return true;
      }
    }
  }
}
