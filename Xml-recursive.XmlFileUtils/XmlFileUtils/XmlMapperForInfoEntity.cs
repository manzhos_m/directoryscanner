﻿using System.Xml.Linq;
using Models.Info;

namespace XmlFileUtils
{
  public static class XmlMapperForInfoEntity
  {
    public const string FolderElemName = "Folder";
    public const string FileElemName = "File";
    public const string NameAttributeName = "Name";
    public const string CreationDateUtcAttributeName = "CreationDateUtc";
    public const string ModificationDateUtcAttributeName = "ModificationDateUtc";
    public const string LastAccessDateUtcAttributeName = "LastAccessDateUtc";
    public const string AttributesAttributeName = "Attributes";
    public const string SizeAttributeName = "Size";
    public const string OwnerAttributeName = "Owner";
    public const string FileSystemRightsAttributeName = "FileSystemRights";

    public static XElement ConvertToXElement(BaseInfoEntity entity)
    {
      XElement element;

      if (entity is DirectoryInfoEntity)
      {
        element = new XElement(FolderElemName);
      }
      else if (entity is FileInfoEntity)
      {
        element = new XElement(FileElemName);
      }
      else
      {
        element = new XElement("Undefined");
      }

      element.SetAttributeValue(NameAttributeName, entity.Name);
      element.SetAttributeValue(CreationDateUtcAttributeName, entity.CreationDateUtc.ToString("O"));
      element.SetAttributeValue(ModificationDateUtcAttributeName, entity.ModificationDateUtc.ToString("O"));
      element.SetAttributeValue(LastAccessDateUtcAttributeName, entity.LastAccessDateUtc.ToString("O"));
      element.SetAttributeValue(AttributesAttributeName, entity.Attributes.ToString("F"));
      element.SetAttributeValue(SizeAttributeName, entity.Size);
      element.SetAttributeValue(OwnerAttributeName, entity.Owner);
      element.SetAttributeValue(FileSystemRightsAttributeName, entity.FileSystemRights.ToString("F"));
      return element;
    }
  }
}