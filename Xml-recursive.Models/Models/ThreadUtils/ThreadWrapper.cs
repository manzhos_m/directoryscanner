﻿using System;
using System.Threading;
using Models.Info;

namespace Models.ThreadUtils
{
  public class ThreadWrapper<T> where T : class
  {
    private readonly Action<T> _callback;
    private readonly Func<object, T> _method;

    private readonly object _inputData;
    private readonly Thread _thread;

    public ThreadWrapper(object inputData, Func<object, T> method, Action<T> callback = null, bool isBackground = true)
    {
      _inputData = inputData;
      _method = method;
      _callback = callback;
      _thread = new Thread(new ParameterizedThreadStart(ThreadProcess));
      _thread.IsBackground = isBackground;
    }

    public void Start()
    {
      _thread.Start(_inputData);
    }
    
    private void ThreadProcess(object inputData)
    {
      var returnValue = _method(inputData);

      if (_callback != null)
      {
        _callback.Invoke(returnValue);
      }

      _thread.Abort();
    }
  }
}
