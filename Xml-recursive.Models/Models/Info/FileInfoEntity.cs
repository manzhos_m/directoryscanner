﻿using System;
using System.IO;
using System.Security.AccessControl;

namespace Models.Info
{
  public class FileInfoEntity : BaseInfoEntity
  {
    public FileInfoEntity(FileInfo fileInfo, string owner, FileSystemRights rights, long fileSize)
      : base(name: fileInfo.Name, creationDateUtc: fileInfo.CreationTimeUtc, modificationDateUtc: fileInfo.LastWriteTimeUtc,
        lastAccessDateUtc: fileInfo.LastAccessTimeUtc, attributes: fileInfo.Attributes, size: fileSize, owner: owner, fileSystemRights: rights)
    {
    }
  }
}
