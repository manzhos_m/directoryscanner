﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using Models.Collections;

namespace Models.Info
{
  public class DirectoryInfoEntity : BaseInfoEntity
  {
    private readonly List<FileInfoEntity> _childFiles;
    private readonly List<DirectoryInfoEntity> _childDirectories;

    public ReadOnlyCollection<DirectoryInfoEntity> ChildDirectories
    {
      get { return _childDirectories.ToList().AsReadOnly(); }
    }

    public ReadOnlyCollection<FileInfoEntity> ChildFiles
    {
      get { return _childFiles.ToList().AsReadOnly(); }
    }

    public DirectoryInfoEntity(DirectoryInfo directoryInfo, string owner, FileSystemRights rights)
      : base(name: directoryInfo.Name, creationDateUtc: directoryInfo.CreationTimeUtc, modificationDateUtc: directoryInfo.LastWriteTimeUtc,
        lastAccessDateUtc: directoryInfo.LastAccessTimeUtc, attributes: directoryInfo.Attributes, size: 0, owner: owner, fileSystemRights: rights)
    {
      _childDirectories = new List<DirectoryInfoEntity>();
      _childFiles = new List<FileInfoEntity>();
    }

    public void RegisterNewFile(FileInfoEntity newFile)
    {
      Size += newFile.Size;
      _childFiles.Add(newFile);
    }

    public void RegisterNewDirectory(DirectoryInfoEntity newDirectory)
    {
      Size += newDirectory.Size;
      _childDirectories.Add(newDirectory);
    }
  }
}
