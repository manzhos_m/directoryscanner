﻿using System;
using System.IO;
using System.Security.AccessControl;

namespace Models.Info
{
  public abstract class BaseInfoEntity 
  {
    public string Name { get; }
    public DateTime CreationDateUtc { get; }
    public DateTime ModificationDateUtc { get; }
    public DateTime LastAccessDateUtc { get; }
    public FileAttributes Attributes { get; }
    public string Owner { get; }
    public FileSystemRights FileSystemRights { get; }
    public long Size { get; protected set; }

    public BaseInfoEntity(string name, DateTime creationDateUtc, DateTime modificationDateUtc, 
      DateTime lastAccessDateUtc, FileAttributes attributes, long size, string owner, FileSystemRights fileSystemRights)
    {
      Name = name;
      CreationDateUtc = creationDateUtc;
      ModificationDateUtc = modificationDateUtc;
      LastAccessDateUtc = lastAccessDateUtc;
      Attributes = attributes;
      Owner = owner;
      FileSystemRights = fileSystemRights;
      Size = size;
    }
  }
}
