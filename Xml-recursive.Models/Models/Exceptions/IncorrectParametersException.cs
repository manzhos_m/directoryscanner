﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Exceptions
{
  public class IncorrectParametersException : Exception
  {
    public IncorrectParametersException()
    {
    }

    public IncorrectParametersException(string message) : base(message)
    {
    }
  }
}
