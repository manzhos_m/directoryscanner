﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Collections
{
  public class QueueWithLastField<T> : Queue<T>
  {
    public T Last { get; private set; }

    public new void Enqueue(T item)
    {
      Last = item;
      base.Enqueue(item);
    }
  }
}
